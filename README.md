### DANIEL SHAMANY

| [Phone](323.475.8223) | [Email](dshamany@gmail.com) | [Website](https://danielshamany.com) | [LinkedIn](https://linkedin.com/in/dshamany) | [GitLab](https://gitlab.com/dshamany) | [GitHub](https://github.com/dshamany) |
| --- | --- | --- | --- | --- | --- |

### _Experience_

**Software Engineer -** _**QuayChain Technologies**_ **\[2022-2023\]**

*   Building backend infrastructure to capture real-time data from IoT edge devices
*   Develop APIs using AWS Lambda, AWS Gateway, and DynamoDB for capturing data
*   Utilize AWS SQS, AWS SES, and more to facilitate complex API interactions for low latency

**Interim CTO -** _**Stealth Startup**_ **\[2021-Present\]**

*   Developing a technology-driven team ready to tackle tomorrow's challenges
*   Procuring technologies to help develop next generation products in the IoT and cloud space
*   Consulting on strategies to develop and deploy targeted IoT devices

**Software Engineer -** _**Caliber Alpha**_ **\[2018-2021\]**

*   Created full stack (MERN) applications, and utilized containers to run and deploy projects in cloud systems like AWS, Azure and GCP
*   Developed front end applications in Angular and Nest.js while utilizing OCI (Docker) containers
*   Created applications that utilize Python and Django for prototyping smaller projects

**Software Engineer -** _**Clean Initiative**_ **\[2017-2019\]**

*   Developed iOS applications for iPads and iPhones using Swift and XCode
*   Worked with Linux tools on EC2 clusters and S3 buckets for application back end
*   Created the onboarding system for employees to deliver setup and badges effectively

| Years | Technologies |
| --- | --- |
| **\< 1** | Angular, Nest.js, Kubernetes, Rust |
| **1-3** | PostgreSQL, Passport.js, JWT,, Qt |
| **3-5** | React.js, React Native, Python, Django, Node.js, Express.js, MongoDB, MariaDB, MySQL, REST APIs, Git, Agile Development, Test-Driven Development, AWS (S3, SQS, DynamoDB, API Gateway, Lambda, EC2), bcrypt, Docker |
| **5-10** | C++, JavaScript, Object-Oriented Programming, Data Structures, Algorithms, |
| **\> 10** | Linux (Fedora, RHEL, Ubuntu), MacOS, Windows, Bash, Vim |

### _Education_

| Degree | Institution | Location | Graduation |
| --- | --- | --- | --- |
| **B.S. Business Management** | _California State University_ | Northridge, CA | 2015 |
| **Cert. Software Engineering** | _General Assembly_ | Santa Monica, CA | 2019 |